Feature: User Management

  User Story:
  In order to manage users
  As an administrator
  I need CRUD permissions to user accounts

  Rules:
   - Non-admin users have no permissions over other accounts
   - There are two types of non-admin accounts:
   -- Risk managers
   -- Assets managers

  Questions:
   - Do admin users need access etc...

  To do:
   - Force reset

  Domain language:
  Group = users permissions are defined by the group they belong to
  CRUD = Create Read Update Delete
  Admin permissions = access to CRUD risks, users and assets for all groups


  Background:
    Given a user called simon with administrator permissions and password S@feb3ar
    And a user called tom with risk_management permissions and password S@feb3ar

  @high-impact
  Scenario Outline: The administrator checks a user's details
    When simon is logged in with password <password>
    Then he is able to view <user>'s account
    Examples:
      | password | user |
      |S@feb3ar  |tom   |

  @high-risk
  @to-do
  Scenario Outline: A user's password is reset by the admin
    Given a <user> has lost his password
    When simon is logged in with password S@feb3ar
    Then simon can reset <user>'s password
    Examples:
      | user |
      |tom   |






